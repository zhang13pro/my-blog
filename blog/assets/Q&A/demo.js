// 20201118
class Foo {
  #count = 0;
  log() {
    console.log(this.#count);
  }
  static say(name) {
    console.log(`Hello ${name}`);
  }
}
let foo = new Foo();
foo.log(); // 私有属性只能在类内部访问
Foo.say("Jiffy"); // 静态方法不能被实例访问

// 20201120
console.log(Object.getOwnPropertyNames(Foo));
