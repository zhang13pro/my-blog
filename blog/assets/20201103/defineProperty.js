let aValue;
let obj = {};
Object.defineProperty(obj, "b", {
  get() {
    console.log("监听正在获取a");
    return aValue;
  },
  set(newValue) {
    console.log("监听正在设置a");
    aValue = newValue;
  },
  enumerable: true,
  configurable: true,
});
// obj.b = 38;
// console.log(obj.b);
obj.b;
obj.b = 40;

let des = Object.getOwnPropertyDescriptor(obj, "b");
console.log(des);

// second 2020-12-17
let demo = {
  a: 10,
};
Object.defineProperty(demo, "a", {
  enumerable: true,
  configurable: true,
  get() {
    console.log("getter...");
    return demo.a;
  },
  set(newValue) {
    console.log("setter...");
    demo.a = newValue;
  },
});

// TODO
// isStackOverflowError(e)
demo.a = 11;
