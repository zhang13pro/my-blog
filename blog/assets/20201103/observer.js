class Observer {
  constructor() {
    // we need container collect regist imformation
    this.dep = [];
  }

  regist(fn) {
    this.dep.push(fn);
  }

  notify() {
    this.dep.forEach((item) => item());
  }
}
