function defineReactive(data, key, val) {
  Object.defineProperty(data, key, {
    enumerable: true,
    configurable: true,
    get() {
      return val;
    },
    set(newValue) {
      if (val === newValue) {
        return;
      }
      val = newValue;
    },
  });
}
