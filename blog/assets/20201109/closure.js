function sum() {
  var x = 10,
    y = 10;
  return function test(z) {
    return x + y + z;
  };
}
console.log(sum.name);
// sum(...).test is not a function
console.log(sum().test(10));
// 30
console.log(sum()(10));

function add() {
  let x = 101;
  return {
    set(value) {
      x = value;
    },
    get() {
      return x;
    },
  };
}
let api = add();
console.log(add.x); // undefined
console.log(api.get()); // 101
api.set(10);
console.log(api.get()); // 10

console.log((() => {}).name); // 空字符串
console.log(new Function().name); // anonymous
