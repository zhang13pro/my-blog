class Logger {
  printName(name = "Jiffy") {
    this.print(`Hello ${name}`);
  }
  print(text) {
    console.log(text);
  }
}

const logger = new Logger();
logger.printName();
const { printName } = logger;
printName();
