---
title: 我的博客应该是怎样的
date: 2020-11-17
tags:
  - Todo
summary: 本文仅仅记录我那随时产生想法又懒得去实现的大脑产物。
author: Jiffy
location: Hangzhou
---

## 20201117-parseInt

- 突然，我又想着我得博客应该好看一点。
- 最好多个 Todo 功能，好让我方便记录要完成得事项。
- 再多个 Q/A 分类，帮我记着那些不急需解决的问题。

Q: 字符串类型转整数类型

```js
console.log(parseInt("012")); // 12
console.log(parseInt("0x12")); // 18
```

上面第一行输出`12`，那我想输出 8 进制的 `012` 值怎么办？

A: 没想到这么快就找到答案了，很简单。`parseInt`API 可接收第二个参数作为指定解析进制。

```js
console.log(parseInt("012", 8)); // 10
```

## 20201118-私有属性

Q: JavaScript 的私有属性怎么表示

```js
class Foo {
  #count = 0;
  log() {
    console.log(this.#count);
  }
  static say(name) {
    console.log(`Hello ${name}`);
  }
}
let foo = new Foo();
foo.log(); // 私有属性只能在类内部访问
Foo.say("Jiffy"); // 静态方法不能被实例访问
```

A: 通过`#`修饰符

## 20201120

Q: JS 获取对象属性
