---
title: ES6 的使用
date: 2020-11-12
tags:
  - JavaScript
summary: ES6 的 Class、Promise
author: Jiffy
location: Hangzhou
---

```js
class Point {
  // ...
}

typeof Point; // "function"
Point === Point.prototype.constructor; // true
```

```JS
class Point {
  // constructor() {}
}
```

```JS
class Foo {
  constructor() {
    return Object.create(null);
  }
}

Foo()
// TypeError: Class constructor Foo cannot be invoked without 'new'
```

## JavaScript 原型
