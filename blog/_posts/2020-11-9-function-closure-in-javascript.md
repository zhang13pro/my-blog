---
title: 怎么理解闭包
date: 2020-11-9
tags:
  - JavaScript
summary: 从我决定入前端这个坑开始，闭包就像一只拦路虎，是时候会会它了，奥里给。
author: Jiffy
location: Hangzhou
---

JavaScript 中最有意思的部分无疑是函数了，因为函数也是一个对象，所以函数名其实是指向函数对象的指针。每个`function`都是`Function`类型的实例。
函数通常以[函数声明](TODO 函数声明和表达式)的方式定义：
但是 JavaScript 中的函数是不能直接调用内部方法的。
类似 Java 中使用 `setter` 和 `getter` 访问私有变量，JavaScript 只能通过闭包访问函数对象的内部属性。也就是说，直接通过对象的`.`修饰符访问属性是无效的。

```js
function sum() {
  var x = 10,
    y = 10;
  return function test(z) {
    return x + y + z;
  };
}
// sum(...).test is not a function
console.log(sum().test(10));
// 30
console.log(sum()(10));
```

类似的，所以闭包就是函数提供接口暴露给外部修改私有属性的手段，例如：

```js
function add() {
  let x = 101;
  return {
    set(value) {
      x = value;
    },
    get() {
      return x;
    },
  };
}
let api = add();
console.log(add.x); // undefined 不能直接访问内部属性
console.log(api.get()); // 101
api.set(10);
console.log(api.get()); // 10
```

在 JavaScript 中，函数只是一段静态的代码、脚本文件，是一个代码书写时、编译期静态的概念；而闭包
