---
title: 一路向我们走来的JavaScript
date: 2020-11-7
tags:
  - JavaScript
  - history
summary: 让我们来看看仅仅怀孕10天的早产儿的成长史。
author: Jiffy
location: Hangzhou
---

## 回顾

早先，网页的表单数据验证需要与服务器通信，这无疑是很慢的。JavaScript 就是为了解决这个问题出现的。1995 年，网景公司的工程师 Brendan Eich 为即将发布的 Netscape Navigator2 开发一个叫 Mocha 的脚本语言，后更名为 LiveScript。为了赶上发布时间，网景与 Sun 公司共同完成 LiveScript 的开发并在 Netscape Navigator2 正式发布前改名为 JavaScript，目的是想蹭时下 Java 的热度。

1996 年 8 月，微软着力进入 Web 浏览器领域，在 IE3 中包含了名为 JScript 的 JavaScript 实现。与 C 语言等不同，JavaScript 并没有语法规范和标准，因此在 1997 年第 39 技术委员会花数月打造出 ECMA-262，也就是 ECMAScript 这个新的脚本语言标准。1998 年，随着 ISO 和 IEC 将 ECMAScript 采纳为标准（ISO/IEC-16262），各家浏览器均以 ECMAScript 作为自己 JavaScript 实现的依据，但具体实现各有不同。

## JavaScript

JavaScript 和 ECMAScript 基本算是同义词，但 JavaScript 实现更为广泛，具体可分为：

- 核心（ECMAScript）
- 文档对象模型（DOM）
- 浏览器对象模型（BOM）

### ECMAScript

ECMAScript 其实是 ECMA-262 定义的语言，并不局限于 Web 浏览器。ECMA-262 将这门语言作为一个基准来定义，以便在它之上再构建更稳健的脚本语言。Web 浏览器只是 ECMAScript 实现的一种宿主环境（host environment）。宿主环境提供 ECMAScript 的基准实现和与环境自身交互必需的扩展。扩展（比如 DOM）使用 ECMAScript 核心类型
和语法，提供特定于环境的额外功能。其他宿主环境还有服务器端 JavaScript 平台 Node.js 和即将被淘汰的 Adobe Flash。

ECMA-262 定义了这门语言的语法、类型、语句、关键字、保留字、操作符和全局对象。ECMAScript 只是对实现这个规范描述的所有方面的一门语言的称呼。JavaScript 实现了 ECMAScript，而 Adobe ActionScript 同样也实现了 ECMAScript。

### DOM

Document Object Model 是一个用于在 HTML 中使用扩展 XML 的 API。它通过一组分层节点表示页面结构，赋能给开发者轻松地对节点增删改，达到随心所欲地控制页面内容和结构地目的。

1998 年 10 月，为了避免各家浏览器公司采用不同的 DHTML（DOMLevel0），W3C(万维网联盟)制定了 DOMLevel1 推荐标准。这个规范由两个模块组成：DOM Core 和 DOM HTML。前者提供了一种映射 XML 文档，从而方便访问和操作文档任意部分的方式；后者扩展了前者，并增加了特定于 HTML 的对象和方法。

> 注意 DOM 并非只能通过 JavaScript 访问，而且确实被其他很多语言实现了。不过对于浏览器来说，DOM 就是使用 ECMAScript 实现的，如今已经成为 JavaScript 语言的一大组成部分。

### BOM

BOM API 用于支持访问和操作浏览器的窗口。HTML5 之前，它是唯一一个没有相关标准的 JavaScript 实现，所以问题很多。BOM 主要针对浏览器窗口和子窗口（frame），通常人们会把任何特定于浏览器的扩展都归在 BOM 的范畴内，for example:

- 弹出新浏览器窗口的能力
- 移动、缩放和关闭浏览器窗口的能力
- navigator 对象，提供关于浏览器的详细信息
- location 对象，提供浏览器加载页面的详细信息
- screen 对象，提供关于用户屏幕分辨率的详细信息
- performance 对象，提供浏览器内存占用、导航行为和时间统计的详细信息
- 对 cookie 的支持 -其他自定义对象，如 XMLHttpRequest 和 IE 的 ActiveXObject

## Ending

JavaScript 作为一门用来与网页交互的脚本语言，虽然只用了 10 天就被创造处来，但是它现在却影响着越来越多身处其中的技术人员和非技术人们。
